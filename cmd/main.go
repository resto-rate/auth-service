// Package classification AuthService API.
// Consumes:
// - application/json
//
// Produces:
// - application/json
//
// Schemes: http
// Host: localhost
// BasePath: /
// Version: 0.0.1
//
// swagger:meta
package main

import (
	"AuthService/internal/config"
	"AuthService/internal/repo"
	"AuthService/internal/repo/repoFunctions"
	"AuthService/internal/server"
	"AuthService/internal/server/handlers"
	"AuthService/internal/server/middleware"
	"github.com/Restream/reindexer"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const Version = "v1.0.0"

func main() {

	logger := logrus.New()
	if logger == nil {
		logger.Fatal("New logger failed")
	}

	// Init configuration and credentials
	configuration, err := config.NewConfiguration()
	if err != nil {
		logger.Fatal("config.NewConfiguration:", err.Error())
	}

	credentials, err := config.NewCredentials()
	if err != nil {
		logger.Fatal("config.NewCredentials:", err.Error())
	}

	// Соединение с БД Reindexer
	DBReindexerConn, err := repo.NewReindexerConnect(credentials.Reindexer)
	if err != nil {
		logger.WithError(err).Fatal("newReindexerConnect - error")
	}

	repositoryRegistry := InitRepoRegistry(DBReindexerConn)

	route := InitHandlers(InitHandlersDep{
		RepositoryRegistry: repositoryRegistry,
		Conf:               configuration,
		Credentials:        credentials,
		Logger:             logger,
	})

	srv := server.NewServer(server.Options{
		ReadTimeout:       configuration.WebServer.Configuration.ReadTimeout,
		WriteTimeout:      configuration.WebServer.Configuration.WriteTimeout,
		ShutdownTimeout:   configuration.WebServer.Configuration.ShutdownTimeout,
		ReadHeaderTimeout: configuration.WebServer.Configuration.ReadHeaderTimeout,
		IdleTimeout:       configuration.WebServer.Configuration.IdleTimeout,
		MaxHeaderBytes:    configuration.WebServer.Configuration.MaxHeaderBytes,
	}, server.Dependencies{
		Handler: route,
		Logger:  logger,
		Config:  configuration,
	})
	go func() {
		err := srv.Start()
		if err != nil {
			logger.Fatalf("failed to Start server; err - %v", err)
		}
	}()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-done
	logger.Error("Server Stopped")

	err = srv.Stop()
	if err != nil {
		logger.Error("failed to correct Stop server; err - %v", err)
	}
	time.Sleep(time.Second * 3)
	logger.Error("Server Exited Properly")

	// Init db and repos
	/*db, err := reindexer.New(&reindexer.Config{
		Dsn: credentials.Reindexer.Dsn,
	})
	if err != nil {
		log.Fatalln("reindexer.New:", err.Error())
	}

	userrepo, err := reindexer.NewUser(&reindexer.UserDependencies{
		DB: db,
	})
	if err != nil {
		log.Fatalln("reindexer.NewUser:", err.Error())
	}

	// Init usecases
	userUsecase, err := usecases.NewUser(&usecases.UserDependencies{
		Repo: userrepo,
	})
	if err != nil {
		log.Fatalln("usecases.NewUser:", err.Error())
	}

	syncworker, err := syncapp.New(
		&syncapp.Dependencies{
			Usecase: userUsecase,
		},
		&syncapp.Config{
			Interval: configuration.Sync.Interval,
		},
	)
	if err != nil {
		log.Fatalln("syncapp.New:", err.Error())
	}

	api1cSync, err := syncapp.NewApi1cSync(
		&syncapp.DependenciesApi1cSync{
			Api: api1c,
		},
		&syncapp.Api1cSyncConfig{},
	)
	if err != nil {
		log.Fatalln("NewApi1cSync:", err.Error())
	}

	api1cSync.Add(api1c.GetLogists1C)
	api1cSync.Add(api1c.GetTransportManagers1C)

	syncworker.Add(api1cSync)

	/*
		httptransport, err := transport.NewHttpClient()
		if err != nil {
			log.Fatalln(err.Error())
		}


	// Init http server
	/*httpsserver, err := rest.NewHttpServer(
		&rest.HttpServerDependencies{},
		&rest.HttpServerConfig{},
	)
	if err != nil {
		log.Fatalln("NewHttpServer:", err.Error())
	}

	// Init rpcapi server
	grpcserver, err := rpcapi.NewGrpcServer()
	if err != nil {
		log.Fatalln("NewGrpcServer:", err.Error())
	}

	userhandlerrpc := rpcapi.NewUserServer()

	grpcserver.Register(userhandlerrpc)

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	wg := &sync.WaitGroup{}

	wg.Add(1)
	go func() {
		syncworker.Run(ctx)
		wg.Done()
	}()
	/*
		wg.Add(1)
		go func() {
			httpsserver.Run(ctx)
			wg.Done()
		}()


	wg.Add(1)
	go func() {
		// TODO: Add rpcapi server
		grpcserver.Run(ctx)
		wg.Done()
	}()

	<-ctx.Done()

	wg.Wait()*/
}

func InitRepoRegistry(DBConnect *reindexer.Reindexer) *repo.Registry {
	return &repo.Registry{
		AccountsRepo: repoFunctions.NewAccountsRepo(DBConnect),
		OTPRepo:      repoFunctions.NewOTPRepo(DBConnect),
		SessionsRepo: repoFunctions.NewSessionRepo(DBConnect),
	}
}

func InitHandlers(dep InitHandlersDep) http.Handler {

	authHandlers, err := handlers.NewAuthHandlers(handlers.AuthDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init authHandlers")
	}

	otpHandlers, err := handlers.NewOtpHandlers(handlers.OtpDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init routerHandlers")
	}

	requestIDMiddleware := middleware.NewRequestIDMiddleware()

	limiterMiddleware, err := middleware.NewLimiterMiddleware(middleware.LimiterMiddlewareDependencies{
		Logger: dep.Logger,
	}, dep.Conf.PortalSecurity.Limiter.RequestPerSecond, dep.Conf.PortalSecurity.Limiter.MaxOnlineRequest)
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init limiterMiddleware")
	}

	authMiddleware, err := middleware.NewAuthMiddleware(middleware.AuthMiddlewareDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Conf:               dep.Conf,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init authMiddleware")
	}

	profile, err := handlers.NewProfileHandlers(handlers.ProfileDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init profileHandlers")
	}

	middlewares := handlers.Middlewares{
		RequestIDMiddleware: requestIDMiddleware,
		LimiterMiddleware:   limiterMiddleware,
		AuthMiddleware:      authMiddleware,
	}

	handlersApi := handlers.Handlers{
		Profile: profile,
	}

	route := handlers.NewRoute(handlers.RouteDependencies{
		Logger:      dep.Logger,
		Middlewares: middlewares,
		Handlers:    handlersApi,
		Otp:         otpHandlers,
		Auth:        authHandlers,
		Conf:        dep.Conf,
	}, handlers.RouteOptions{
		Version: Version,
		Cors:    handlers.RouteOptionsCors{},
	})

	return route
}

type InitHandlersDep struct {
	RepositoryRegistry *repo.Registry
	Conf               *config.Config
	Credentials        *config.Credentials
	Logger             *logrus.Logger
}
