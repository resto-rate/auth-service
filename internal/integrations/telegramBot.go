package integrations

import (
	"fmt"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

const (
	sendMessageTgBotUrl = "/sendMessage"
	markDownV2          = "MarkdownV2"
	tgAddress           = "https://api.telegram.org/bot"
)

func SendOtpToTgBot(chatID, otp string) (*http.Response, error) {

	viper.SetEnvPrefix("bot")
	err := viper.BindEnv("token")
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s%s?chat_id=%s&text=Ваш одноразовый код: `%s`&parse_mode=%s",
		tgAddress, viper.GetString("token"), sendMessageTgBotUrl, chatID, otp, markDownV2), nil)
	if err != nil {
		return nil, err
	}

	client := http.Client{Timeout: time.Second * 20}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}
