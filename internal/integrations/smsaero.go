package integrations

import (
	"fmt"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

func SendSms(phoneNumber, otpCode string) (*http.Response, error) {
	viper.SetEnvPrefix("smsaero")
	err := viper.BindEnv("apikey")
	if err != nil {
		return nil, err
	}

	err = viper.BindEnv("email")
	if err != nil {
		return nil, err
	}

	reqStr := fmt.Sprintf("https://%s:%s@gate.smsaero.ru/v2/sms/send?number=7%s&text=Ваш+код+для+входа:+%s&sign=SMS Aero",
		viper.GetString("email"), viper.GetString("apikey"), phoneNumber, otpCode)
	req, err := http.NewRequest("GET", reqStr, nil)
	if err != nil {
		return nil, err
	}

	client := http.Client{Timeout: time.Second * 20}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}
