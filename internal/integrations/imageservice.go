package integrations

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"time"
)

const (
	imageUploadUrl = "/image/upload"
)

func RedirectUploadImageFile(url string, r *http.Request) (*http.Response, error) {

	file, h, err := r.FormFile("imageFile")
	if err != nil {
		return nil, err
	}
	defer func(file multipart.File) {
		err := file.Close()
		if err != nil {

		}
	}(file)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	fw, err := writer.CreateFormFile("imageFile", h.Filename)
	if err != nil {
		return nil, err
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		return nil, err
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s%s", url, imageUploadUrl), bytes.NewReader(body.Bytes()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())

	client := http.Client{Timeout: time.Second * 20}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}
