package convert

import (
	"AuthService/internal/repo/repoModels"
	"AuthService/internal/server/models"
)

func SessionCreateRepo2Client(session *repoModels.Session, account *repoModels.Account) *models.LoginResponse {
	return &models.LoginResponse{
		AccountGUID:   session.AccountGUID,
		AccountStatus: account.Status,
		At: models.Token{
			Token: session.CookiesKey,
			Exp:   session.ExpiredTime,
		},
		Rt: models.Token{
			Token: session.RefreshToken,
			Exp:   session.RefreshTokenExp,
		},
	}
}

func SessionRefreshRepo2Client(session *repoModels.Session) *models.RefreshResponse {
	return &models.RefreshResponse{
		AccountGUID: session.AccountGUID,
		At: models.Token{
			Token: session.CookiesKey,
			Exp:   session.ExpiredTime,
		},
		Rt: models.Token{
			Token: session.RefreshToken,
			Exp:   session.RefreshTokenExp,
		},
	}
}

func AccountRepo2Client(account *repoModels.Account) *models.AccountResponse {
	return &models.AccountResponse{
		AccountGUID: account.AccountGUID,
		Role:        account.Role,
		Login:       account.Login,
		Mobile:      account.Mobile,
		Name:        account.Name,
		Surname:     account.Surname,
		ImageURL:    account.ImageURL,
		RestAdmin:   account.RestAdmin,
		City:        account.City,
	}
}

func Cities2Client(cities []string) *models.CitiesResponse {
	res := &models.CitiesResponse{
		Cities: []string{},
	}

	for _, city := range cities {
		res.Cities = append(res.Cities, city)
	}

	return res
}

func User2Client(account *repoModels.Account) *models.UserResponse {
	return &models.UserResponse{
		AccountGUID: account.AccountGUID,
		Name:        account.Name,
		Surname:     account.Surname,
		ImageURL:    account.ImageURL,
	}
}

func Users2Client(users repoModels.AccountGUIDsSlice) *models.UsersResponse {
	res := &models.UsersResponse{}
	res.Users = make([]string, 0)
	for _, user := range users {
		res.Users = append(res.Users, string(user))
	}

	return res
}
