package convert

func ImageServiceUploadImageRes2Client(bodyBytes []byte) string {
	if len(bodyBytes) > 0 && bodyBytes[0] == '"' {
		bodyBytes = bodyBytes[1:]
	}

	if len(bodyBytes) > 0 && bodyBytes[len(bodyBytes)-1] == '"' {
		bodyBytes = bodyBytes[:len(bodyBytes)-1]
	}

	return string(bodyBytes)
}
