package errproc

import (
	"AuthService/internal/repo/repoModels"
	"errors"
	"fmt"
	"github.com/getsentry/sentry-go"
	"github.com/sirupsen/logrus"
	"net/http"
)

type HandlerErrProc struct {
	logrusEntry logrus.FieldLogger
	xRequestId  *string
	handler     *string
	sentryHub   *sentry.Hub
	account     *handlerAccountData

	requestBody *[]byte
}

type handlerAccountData struct {
	userID   string
	login    string
	username string
	role     string
}

// Возвращает Logger обогащённый xRequestID
func (o *ErrProc) NewHandlerLogger(r *http.Request, logger logrus.FieldLogger, handlerName string) (logrus.FieldLogger, *HandlerData) {
	xRequestID, err := o.getXRequestIDFromHTTPContext(r)
	if err != nil {
		logger.Warning("xRequestID is null")
	}

	fields := make(logrus.Fields)
	fields["handler"] = handlerName

	if xRequestID != nil {
		fields["xRequestId"] = *xRequestID
	}

	handlerData := &HandlerData{
		xRequestId: xRequestID,
		handler:    &handlerName,
	}

	return logger.WithFields(fields), handlerData
}

type HandlerData struct {
	xRequestId *string
	handler    *string
}

func (o *ErrProc) NewHandlerErrProc(r *http.Request, logger logrus.FieldLogger, handlerData *HandlerData) (*HandlerErrProc, error) {

	sentryHub, err := o.getSentryHubFromHTTPContext(r)
	if err != nil {
		return nil, err
	}

	if logger == nil {
		return nil, errors.New("init handlerErrProc - error: logger is null")
	}

	handlerErrProc := &HandlerErrProc{
		logrusEntry: logger,
		xRequestId:  nil,
		handler:     nil,
		sentryHub:   sentryHub,
		account:     nil,
		requestBody: nil,
	}

	if handlerData != nil {
		handlerErrProc.xRequestId = handlerData.xRequestId
		handlerErrProc.handler = handlerData.handler
	}

	return handlerErrProc, nil
}

func (o *HandlerErrProc) AddAccountData(a *repoModels.Account) {
	account := &handlerAccountData{
		userID:   a.AccountGUID,
		login:    a.Login,
		username: fmt.Sprintf("%s %s", a.Name, a.Surname),
		role:     a.Role,
	}
	o.account = account
}

func (o *HandlerErrProc) NewException(err error) *HandlerException {

	if o.account != nil {
		o.sentryHub.ConfigureScope(func(scope *sentry.Scope) {

			data := make(map[string]string)
			data["Role"] = o.account.role

			scope.SetUser(sentry.User{
				ID:       o.account.userID,
				Username: o.account.username,
				Name:     o.account.login,
				Data:     data,
			})
		})
	}

	if o.handler != nil {
		o.sentryHub.ConfigureScope(func(scope *sentry.Scope) {
			scope.SetTag("handler", *o.handler)
		})
	}

	if o.requestBody != nil {
		o.sentryHub.ConfigureScope(func(scope *sentry.Scope) {
			scope.SetRequestBody(*o.requestBody)
		})
	}

	logger := o.logrusEntry.WithError(err)

	return &HandlerException{
		sentryHub: o.sentryHub,
		EventID:   nil,
		Logger:    logger,
		Err:       &err,
		Wrap:      nil,
	}
}

type HandlerException struct {
	sentryHub *sentry.Hub
	EventID   *sentry.EventID
	Logger    *logrus.Entry
	Err       *error
	Wrap      *string
}

func (o *HandlerException) CaptureSentry() *HandlerException {

	if o.Wrap != nil {
		o.sentryHub.ConfigureScope(func(scope *sentry.Scope) {
			scope.SetTag("wrap", *o.Wrap)
		})
	}

	eventID := o.sentryHub.CaptureException(*o.Err)
	// TODO: Добавить при подключении setnry eventid
	logrusEntry := o.Logger.WithField("eventid", "")

	return &HandlerException{
		sentryHub: o.sentryHub,
		EventID:   eventID,
		Logger:    logrusEntry,
		Err:       o.Err,
		Wrap:      o.Wrap,
	}
}

func (o *HandlerException) ConsolePrint() {
	if o.Wrap != nil {
		o.Logger.WithError(*o.Err).Error(*o.Wrap)
	} else {
		o.Logger.Error(*o.Err)
	}
}
