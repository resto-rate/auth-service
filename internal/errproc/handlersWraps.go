package errproc

import "errors"

// Оборацивает исключение
func (o *HandlerException) WrapSet(text string) *HandlerException {
	o.Wrap = &text

	if *o.Err == nil {
		err := errors.New(text)
		o.Err = &err
	}

	return o
}

// Ошибка HTTP-Контекста
func (o *HandlerException) WrapCtxGetAccountError() *HandlerException {
	return o.WrapSet("ctx_get_account_error")
}

// Несоответствие роли пользователя
func (o *HandlerException) WrapAccountRoleMismatch() *HandlerException {
	return o.WrapSet("account_role_mismatch")
}

func (o *HandlerException) WrapMissingArguments() *HandlerException {
	return o.WrapSet("missing_arguments")
}

// Пользователь не является владельцем
func (o *HandlerException) WrapAccountIsNotOwner() *HandlerException {
	return o.WrapSet("account_is_not_owner")
}

// Объект не найден в БД
func (o *HandlerException) WrapRepoNotFound() *HandlerException {
	return o.WrapSet("not_found")
}

// Объект не найден в БД
func (o *HandlerException) WrapRepoError() *HandlerException {
	return o.WrapSet("repo_error")
}

func (o *HandlerException) WrapInvalidPhoneError() *HandlerException {
	return o.WrapSet("invalid_phone_number_error")
}

func (o *HandlerException) WrapMtsSendError() *HandlerException {
	return o.WrapSet("mts_send_error")
}

func (o *HandlerException) WrapConvertError() *HandlerException {
	return o.WrapSet("convert_error")
}

// Ошибка чтения тела запроса
func (o *HandlerException) WrapRequestBodyReadError() *HandlerException {
	return o.WrapSet("request_body_read_error")
}

// Ошибка json.Unmarshal
func (o *HandlerException) WrapJSONUnmarshalError() *HandlerException {
	return o.WrapSet("json_unmarshal_error")
}

// Ошибка json.Marshal
func (o *HandlerException) WrapJSONMarshalError() *HandlerException {
	return o.WrapSet("json_marshal_error")
}

// Ошибка json.Unmarshal
func (o *HandlerException) WriteJSONResponceError() *HandlerException {
	return o.WrapSet("write_json_responce_error")
}

// Ошибка конвертации
func (o *HandlerException) ModelConversionError() *HandlerException {
	return o.WrapSet("model_conversion_error")
}

// Ошибка получение переменной из пути запроса
func (o *HandlerException) WrapHttpPathVarsError() *HandlerException {
	return o.WrapSet("http_path_vars_error")
}

func (o *HandlerException) WrapSchemeDecode() *HandlerException {
	return o.WrapSet("scheme_decode_error")
}

func (o *HandlerException) WrapLinkObjectIsWrong() *HandlerException {
	return o.WrapSet("linkobject_is_wrong")
}

func (o *HandlerException) WrapEmptyFields() *HandlerException {
	return o.WrapSet("data_has_empty_fields")
}

func (o *HandlerException) WrapBlackListHasIt() *HandlerException {
	return o.WrapSet("unit_is_in_blacklist")
}

func (o *HandlerException) WrapRequestsQty() *HandlerException {
	return o.WrapSet("too_many_requests")
}

func (o *HandlerException) WrapSmsProviderErr() *HandlerException {
	return o.WrapSet("sms_provider_error")
}

func (o *HandlerException) WrapGetTransferDataErr() *HandlerException {
	return o.WrapSet("get_transfer_data_error")
}

func (o *HandlerException) WrapOtpAttemptsQty() *HandlerException {
	return o.WrapSet("otp_attempts_quantity_error")
}

func (o *HandlerException) WrapWrongOtpCode() *HandlerException {
	return o.WrapSet("wrong_otp_code")
}

func (o *HandlerException) WrapSessionCompareRequest() *HandlerException {
	return o.WrapSet("request_data_is_not_equal_sessions")
}

func (o *HandlerException) WrapCarGUIDsCompare() *HandlerException {
	return o.WrapSet("carGUIDs_arent_the_same")
}

func (o *HandlerException) WrapDriverGUIDsCompare() *HandlerException {
	return o.WrapSet("driverGUIDs_arent_the_same")
}

func (o *HandlerException) WrapOrderPointsQty() *HandlerException {
	return o.WrapSet("orders_points_have_wrong_quantity")
}

func (o *HandlerException) WrapCorpRouter() *HandlerException {
	return o.WrapSet("corp_router_error")
}

func (o *HandlerException) WrapYandexNavi() *HandlerException {
	return o.WrapSet("yandex_navigation_error")
}

func (o *HandlerException) WrapApi1CErr() *HandlerException {
	return o.WrapSet("api1c_error")
}

func (o *HandlerException) WrapProcessorErr() *HandlerException {
	return o.WrapSet("processor_error")
}

func (o *HandlerException) WrapPrintForm() *HandlerException {
	return o.WrapSet("print_form_error")
}

func (o *HandlerException) WrapWriteErr() *HandlerException {
	return o.WrapSet("write_error")
}

func (o *HandlerException) WrapReadBodyErr() *HandlerException {
	return o.WrapSet("read_body_error")
}

func (o *HandlerException) WrapActsLimit() *HandlerException {
	return o.WrapSet("acts_limit_error")
}

func (o *HandlerException) WrapRefreshTokenExp() *HandlerException {
	return o.WrapSet("refresh_token_is_expired")
}

func (o *HandlerException) WrapStorageStructErr() *HandlerException {
	return o.WrapSet("storage_checking_error")
}

func (o *HandlerException) WrapSessionErr() *HandlerException {
	return o.WrapSet("get_session_from_context_error")
}

func (o *HandlerException) WrapAccountAccessErr() *HandlerException {
	return o.WrapSet("account_access_error")
}

func (o *HandlerException) WrapAuthIsRequired() *HandlerException {
	return o.WrapSet("get_auth_data_from_context")
}

func (o *HandlerException) WrapBindEnvError() *HandlerException {
	return o.WrapSet("bind_env_error")
}

func (o *HandlerException) WrapBadRequest() *HandlerException {
	return o.WrapSet("bad_request_error")
}

func (o *HandlerException) WrapIntegrationError() *HandlerException {
	return o.WrapSet("integration_error")
}

func (o *HandlerException) WrapResponseIntegrationBodyReadError() *HandlerException {
	return o.WrapSet("response_integration_body_read_error")
}

func (o *HandlerException) WrapMultipartFormError() *HandlerException {
	return o.WrapSet("multipart_form_error")
}

func (o *HandlerException) WrapCloseFileError() *HandlerException {
	return o.WrapSet("close_file_error")
}

func (o *HandlerException) WrapUrlParamsErr() *HandlerException {
	return o.WrapSet("url_params_error")
}

func (o *HandlerException) WrapWrongUnloadingDate() *HandlerException {
	return o.WrapSet("wrong_unloading_date")
}

func (o *HandlerException) WrapFuelCardTransfer() *HandlerException {
	return o.WrapSet("fuelcard_transfer_error")
}

func (o *HandlerException) WrapGetOTPError() *HandlerException {
	return o.WrapSet("get_otp_error")
}

func (o *HandlerException) WrapCreateOTPError() *HandlerException {
	return o.WrapSet("create_otp_error")
}

func (o *HandlerException) WrapParseFloat() *HandlerException {
	return o.WrapSet("parse_float_error")
}

func (o *HandlerException) WrapValidation() *HandlerException {
	return o.WrapSet("validation_error")
}

func (o *HandlerException) WrapConfigRestriction() *HandlerException {
	return o.WrapSet("config_restriction")
}

func (o *HandlerException) WrapFtpConnectionErr() *HandlerException {
	return o.WrapSet("ftp_connection_error")
}

func (o *HandlerException) WrapZipWriter() *HandlerException {
	return o.WrapSet("zipwritter_error")
}

func (o *HandlerException) WrapEmptyNotificationUUID() *HandlerException {
	return o.WrapSet("empty_notificationuuid_error")
}

func (o *HandlerException) WrapNotificationGate() *HandlerException {
	return o.WrapSet("notificationgate_error")
}

func (o *HandlerException) WrapRegexp() *HandlerException {
	return o.WrapSet("regexp_error")
}
