package errproc

import (
	"AuthService/internal/server/middleware"
	"errors"
	"fmt"
	"github.com/getsentry/sentry-go"
	"net/http"
)

var (
	ErrHubIsNil = errors.New("hub is nil")
)

type ErrProc struct{}

func NewErrProc() *ErrProc {
	return &ErrProc{}
}

// Возвращает локальный Sentry.Hub запроса, обогащая его самим запросом
func (o *ErrProc) getSentryHubFromHTTPContext(r *http.Request) (*sentry.Hub, error) {
	hub := sentry.GetHubFromContext(r.Context())
	if hub == nil {
		return nil, ErrHubIsNil
	}

	hub.ConfigureScope(func(scope *sentry.Scope) {
		scope.SetRequest(r)
	})

	return hub, nil
}

// Возвращает идетификатор запроса - RequestID из контекста запроса
func (o *ErrProc) getXRequestIDFromHTTPContext(r *http.Request) (*string, error) {
	contextKey := middleware.CtxKeyRequestID
	requestUD, ok := r.Context().Value(contextKey).(*string)
	if !ok {
		return nil, fmt.Errorf("xRequestId is null")
	}
	return requestUD, nil
}
