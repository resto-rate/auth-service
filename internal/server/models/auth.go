package models

// swagger:model AuthMethodsResponse
type AuthMethodsResponse struct {
	OTPEnabled bool `json:"OTPEnabled"`
}

// swagger:model LoginOTPCodeReq
type LoginOTPCodeReq struct {
	Login   string `json:"Login"`
	OtpGUID string `json:"OtpGUID"`
	OTP     string `json:"OTP"`
}

// swagger:model LoginResponse
type LoginResponse struct {
	AccountGUID   string
	AccountStatus int64
	At            Token
	Rt            Token
}

// swagger:model RefreshResponse
type RefreshResponse struct {
	AccountGUID string
	At          Token
	Rt          Token
}

type Token struct {
	Token string `json:"Token"`
	Exp   int64  `json:"Exp"`
}

type LogoutSessionReq struct {
	At string `json:"at"`
}

// swagger:model RefreshSessionReq
type RefreshSessionReq struct {
	LinkObjectGUID string `json:"linkObjectGUID"`
	Rt             string `json:"rt"`
}

// swagger:model ChangeProfileDataReq
type ChangeProfileDataReq struct {
	Name    string `json:"Name"`
	Surname string `json:"Surname"`
	City    string `json:"City"`
}

// swagger:model AccountResponse
type AccountResponse struct {
	AccountGUID string `json:"AccountGUID"`
	Role        string `json:"Role"`
	Login       string `json:"Login"`
	Mobile      string `json:"Mobile"`
	Name        string `json:"Name"`
	Surname     string `json:"Surname"`
	ImageURL    string `json:"ImageURL"`
	RestAdmin   bool   `json:"RestAdmin"`
	City        string `json:"City"`
}

type GetUsersReq struct {
	City string
}

type UsersResponse struct {
	Users []string `json:"Users"`
}

type UserResponse struct {
	AccountGUID string `json:"AccountGUID"`
	Name        string `json:"Name"`
	Surname     string `json:"Surname"`
	ImageURL    string `json:"ImageURL"`
}

type CitiesResponse struct {
	Cities []string `json:"Cities"`
}
