package handlers

import (
	"AuthService/internal/config"
	"AuthService/internal/repo"
	"github.com/sirupsen/logrus"
	"net/http"
)

type ProfileHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
}

type ProfileDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
}

func NewProfileHandlers(dep ProfileDependencies) (*ProfileHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, ErrDepRepoIsNil
	}
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}

	logger := dep.Logger.WithField("", "AuthHandlersCI")

	return &ProfileHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
	}, nil
}

func (o *ProfileHandlers) Profile(w http.ResponseWriter, r *http.Request) {
	/*logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Profile")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, nil, err)
		return
	}

	account, exist := GetAccountFromContext(r.Context())
	if !exist {
		exception := errproc.NewException(ErrAuthIsRequired).WrapAuthIsRequired().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, exception.EventID, ErrAuthIsRequired)
		return
	}
	errproc.AddAccountData(account)

	vars := mux.Vars(r)
	partnerGUID, ok := vars["PartnerGUID"]
	if !ok {
		exception := errproc.NewException(ErrPartnerGUIDIsNotFound).WrapHttpPathVarsError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, exception.EventID, ErrPartnerGUIDIsNotFound)
		return
	}

	if account.LinkObjectGUID != partnerGUID {
		exception := errproc.NewException(ErrPartnerGUIDAndAccountAreDiff).WrapAccountIsNotOwner().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusUnauthorized, exception.EventID, ErrPartnerGUIDAndAccountAreDiff)
		return
	}

	partnerRepo, err := o.repositoryRegistry.PartnersRepo.GetToID(repo.PartnerGUID(partnerGUID))
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, exception.EventID, err)
		return
	}

	otherAccounts := o.repositoryRegistry.GetAllActiveAccountsToLogin(account.Login)

	response := convert.AccounProfileRepo2lk(account, partnerRepo, otherAccounts)

	writeJson(w, logger, response)*/
}
