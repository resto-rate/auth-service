package handlers

import (
	"AuthService/internal/repo/repoModels"
	"AuthService/internal/server/middleware"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
)

var (
	ErrSessionRefreshTokenExpired            = errors.New("refresh token of the session is expired")
	ErrRequestAndSessionLinkedObjAreNotEqual = errors.New("request and session linked objects are not equal")
	ErrManagerOrPartnerGUIDsAreEmpty         = errors.New("managerGUID or partnerGUID are empty")
	ErrLoginDataIsWrong                      = errors.New("user or password are empty")
	ErrOtpNotFound                           = NewErrUnitIsNotFound("OTP code")
	ErrNoAttemptsAnyMore                     = errors.New("no attempts any more")
	ErrOtpIsNotCorrect                       = errors.New("otp code is not correct")
	ErrLoginIsEmpty                          = errors.New("login is empty")
	ErrPhoneNumberIsInvalid                  = errors.New("phone number is invalid")
	ErrInvalidPhoneNumber                    = errors.New("phone number is invalid")
	ErrTooManyPhones                         = errors.New("too many phones")
	ErrAccountIsBlocked                      = errors.New("account is blocked")
	ErrOtpOrLoginAreEmpty                    = errors.New("otp code or login are empty")
	ErrSessionRefresh                        = errors.New("session couldn't do refresh")
	ErrDepRepoIsNil                          = NewErrDepIsNil("RepositoryRegistry")
	ErrDepLoggerIsNil                        = NewErrDepIsNil("Logger")
	ErrDepConfIsNil                          = NewErrDepIsNil("Config")
	ErrAuthIsRequired                        = errors.New("authentication is required")
	ErrBindEnv                               = errors.New("bind env error")
	ErrParseMultiPartForm                    = errors.New("parse multipart form error")
	ErrCloseFile                             = errors.New("close file error")
	ErrRequiredRoleMissing                   = errors.New("required role missing")
	ErrWrongOtpCode                          = errors.New("wrong otp code")
	ErrRequestAndSessionDataAreDiff          = errors.New("request data isn't equal to sessions")
	ErrMtsSendMessage                        = errors.New("send message mts failed")
	ErrIntegrationsRedirect                  = errors.New("redirect to another api error")
)

type ErrDepIsNil struct {
	unit string
}

func GetAccountFromContext(ctx context.Context) (*repoModels.Account, bool) {
	contextKey := middleware.CtxKeyAccount
	u, ok := ctx.Value(contextKey).(*repoModels.Account)
	return u, ok
}

func GetSessionFromContext(ctx context.Context) (*repoModels.Session, bool) {
	contextKey := middleware.CtxKeySession
	s, ok := ctx.Value(contextKey).(*repoModels.Session)
	return s, ok
}

func (err ErrDepIsNil) Error() string {
	return fmt.Sprintf("%s is nil", err.unit)
}

func NewErrDepIsNil(unit string) error {
	return ErrDepIsNil{unit: unit}
}

type ErrUnitIsNotFound struct {
	unit string
}

func (err ErrUnitIsNotFound) Error() string {
	return fmt.Sprintf("%s: not found", err.unit)
}

func NewErrUnitIsNotFound(unit string) error {
	return ErrUnitIsNotFound{unit: unit}
}

type ErrUnitIsMissed struct {
	unit string
}

func (err ErrUnitIsMissed) Error() string {
	return fmt.Sprintf("%s is missed", err.unit)
}

func NewErrUnitIsMissed(unit string) error {
	return ErrUnitIsMissed{unit: unit}
}

func WriteError(w http.ResponseWriter, exterr error, statuscode int) error {
	var err error
	type result struct {
		Result bool   `json:"Result"`
		Error  string `json:"Error"`
	}

	// Формируем JSON
	jbody, err := json.Marshal(result{
		Result: false,
		Error:  fmt.Sprint(exterr),
	})
	if err != nil {
		return err
	}

	// Возвращаем код ошибки и текст ошибки
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statuscode)
	_, err = w.Write(jbody)

	return err
}

// TODO: Добавить sentry
func writeError(w http.ResponseWriter, logger logrus.FieldLogger, statuscode int, err error) {

	type errorStruct struct {
		ErrorID string `json:"ErrorID"`
		Message string `json:"Message"`
	}

	var responseErr errorStruct

	if err != nil {
		responseErr.Message = err.Error()
	}

	responce, err := json.Marshal(responseErr)
	if err != nil {
		logger.Errorf("writeError - error: %w", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statuscode)
	_, err = w.Write(responce)
	if err != nil {
		logger.Errorf("writeError - error: %w", err)
		return
	}
}

func writeJson(w http.ResponseWriter, logger logrus.FieldLogger, body interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	jsonBody, err := json.Marshal(body)
	if err != nil {
		logger.WithError(err).Error("json.Marshal in response - error")
	}

	_, err = w.Write(jsonBody)
	if err != nil {
		logger.WithError(err).Error("w.Write in response - error")
	}
}

func displayRedirectedJson(w http.ResponseWriter, logger logrus.FieldLogger, body []uint8) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	_, err := w.Write(body)
	if err != nil {
		logger.WithError(err).Error("w.Write in response - error")
	}
}
