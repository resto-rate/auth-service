package handlers

import (
	"AuthService/internal/config"
	"AuthService/internal/convert"
	"AuthService/internal/errproc"
	"AuthService/internal/integrations"
	"AuthService/internal/repo"
	"AuthService/internal/repo/repoModels"
	"AuthService/internal/server/models"
	"AuthService/internal/server/utils"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"io"
	"net/http"
	"regexp"
	"time"
)

type AuthHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
	errProc            *errproc.ErrProc
}

type AuthDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
	ErrProc            *errproc.ErrProc
}

func NewAuthHandlers(dep AuthDependencies) (*AuthHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, ErrDepRepoIsNil
	}
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}

	logger := dep.Logger.WithField("AuthService", "AuthHandlersCI")

	authHandlers := &AuthHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
	}

	return authHandlers, nil
}

func (o *AuthHandlers) Health(w http.ResponseWriter, r *http.Request) {
	writeJson(w, nil, "Hello")
}

func (o *AuthHandlers) Methods(w http.ResponseWriter, r *http.Request) {

	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Methods")
	_, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var response models.AuthMethodsResponse

	response.OTPEnabled = o.conf.AuthSettings.OTP.OTPEnabled

	writeJson(w, logger, response)
}

func (o *AuthHandlers) LoginTgBot(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "LoginTgBot")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	fmt.Printf("request: %s", r.Body)

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.TgBotRegReq
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	if jsonRequest.Login == "" {
		exception := errproc.NewException(ErrLoginIsEmpty).WrapEmptyFields().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrLoginIsEmpty)
		return
	}

	if len(jsonRequest.Login) != 10 {
		exception := errproc.NewException(ErrInvalidPhoneNumber).WrapInvalidPhoneError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrTooManyPhones)
		return
	}

	re := regexp.MustCompile(`^\d+$`)
	if !re.MatchString(jsonRequest.Login) {
		exception := errproc.NewException(ErrPhoneNumberIsInvalid).WrapInvalidPhoneError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrPhoneNumberIsInvalid)
		return
	}

	user, err := o.repositoryRegistry.AccountsRepo.GetByLogin(jsonRequest.Login)
	if user != nil {
		user.EnterType = repoModels.EnterTypeTg
		user.ChatID = jsonRequest.ChatID

		err := o.repositoryRegistry.AccountsRepo.Upsert(user)
		if err != nil {
			exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}
		return
	}

	if errors.Is(err, repo.ErrorNotFound) {
		user = &repoModels.Account{
			AccountGUID:     uuid.NewString(),
			Status:          1,
			Login:           jsonRequest.Login,
			Mobile:          jsonRequest.Login,
			Name:            "",
			Surname:         "",
			ImageURL:        "",
			MaxSessionCount: 3,
			Role:            repoModels.RoleTemp,
			TimeZone:        3,
			Blocked:         false,
			EnterType:       repoModels.EnterTypeTg,
			ChatID:          jsonRequest.ChatID,
		}

		err := o.repositoryRegistry.AccountsRepo.Upsert(user)
		if err != nil {
			exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}
	} else if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
}

func (o *AuthHandlers) GetOTPCode(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetOTPCode")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.GetOTPCodeReq
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	if jsonRequest.Login == "" {
		exception := errproc.NewException(ErrLoginIsEmpty).WrapEmptyFields().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrLoginIsEmpty)
		return
	}

	re := regexp.MustCompile(`^\d+$`)
	if !re.MatchString(jsonRequest.Login) {
		exception := errproc.NewException(ErrPhoneNumberIsInvalid).WrapInvalidPhoneError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrPhoneNumberIsInvalid)
		return
	}

	otpPhone := repoModels.OtpPhone(jsonRequest.Login)
	if o.repositoryRegistry.OTPRepo.CountPhone(&otpPhone) > 0 {
		exception := errproc.NewException(ErrTooManyPhones).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrTooManyPhones)
		return
	}

	otpGUID := uuid.NewString()
	if len(jsonRequest.Login) != 10 {
		exception := errproc.NewException(ErrInvalidPhoneNumber).WrapInvalidPhoneError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrTooManyPhones)
		return
	}

	user, err := o.repositoryRegistry.AccountsRepo.GetByLogin(jsonRequest.Login)
	if errors.Is(err, repo.ErrorNotFound) {
		user = &repoModels.Account{
			AccountGUID:     uuid.NewString(),
			Status:          1,
			Login:           jsonRequest.Login,
			Mobile:          jsonRequest.Login,
			Name:            "",
			Surname:         "",
			ImageURL:        "",
			MaxSessionCount: 3,
			Role:            repoModels.RoleTemp,
			TimeZone:        3,
			Blocked:         false,
			EnterType:       repoModels.EnterTypeSms,
		}

		err := o.repositoryRegistry.AccountsRepo.Upsert(user)
		if err != nil {
			exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}
	} else if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	if user.Status != repoModels.AccountsStatus.Active && user.Status != repoModels.AccountsStatus.TempAccount {
		exception := errproc.NewException(ErrAuthIsRequired).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusUnauthorized, ErrAuthIsRequired)
		return
	}

	var otpCode utils.OTP
	if o.conf.AuthSettings.OTP.OTPEnabled {
		otpCode = otpCode.GenerateCode(o.conf.AuthSettings.OTP.Auth.CodeFrom, o.conf.AuthSettings.OTP.Auth.CodeTo)
	} else {
		otpCode = otpCode.DefaultCode()
	}

	expired := time.Now().Unix() + o.conf.AuthSettings.OTP.Auth.ExpiredSeconds
	created := time.Now().Unix()
	err = o.repositoryRegistry.OTPRepo.Create(repoModels.OTP{
		OtpGUID:           otpGUID,
		Phone:             user.Mobile,
		ObjectType:        "",
		OtpCode:           otpCode.Code,
		Created:           created,
		Expired:           expired,
		RemainingAttempts: o.conf.AuthSettings.OTP.MaxRemainingAttempts,
		DeviceID:          "",
		AccountIDs:        []string{user.AccountGUID},
	})
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	if user.EnterType == repoModels.EnterTypeSms {
		fmt.Printf("OTP Code: %s\n", otpCode.CodeSpace)
		_, err := integrations.SendSms(user.Mobile, otpCode.Code)
		if err != nil {
			exception := errproc.NewException(err).WrapIntegrationError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}
	} else if user.EnterType == repoModels.EnterTypeTg {
		if err != nil {
			exception := errproc.NewException(err).WrapBadRequest().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusForbidden, err)
			return
		}

		_, err := integrations.SendOtpToTgBot(user.ChatID, otpCode.Code)
		if err != nil {
			exception := errproc.NewException(err).WrapIntegrationError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusForbidden, err)
			return
		}
	}

	writeJson(w, logger, models.GetOTPCodeRes{
		OtpGUID:            otpGUID,
		AuthExpiredSeconds: o.conf.AuthSettings.OTP.Auth.ExpiredSeconds,
	})
}

func (o *AuthHandlers) LoginOTPCode(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "LoginOTPCode")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.LoginOTPCodeReq
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	if jsonRequest.OTP == "" || jsonRequest.Login == "" {
		exception := errproc.NewException(ErrOtpOrLoginAreEmpty).WrapEmptyFields().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrOtpOrLoginAreEmpty)
		return
	}

	otp, found := o.repositoryRegistry.OTPRepo.Get(repoModels.OtpGUID(jsonRequest.OtpGUID))
	if !found {
		exception := errproc.NewException(ErrOtpNotFound).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusNotFound, ErrOtpNotFound)
		return
	}

	if otp.RemainingAttempts <= 1 {
		exception := errproc.NewException(ErrNoAttemptsAnyMore).WrapOtpAttemptsQty().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrNoAttemptsAnyMore)
		return
	}

	if otp.OtpCode != jsonRequest.OTP {
		exception := errproc.NewException(ErrWrongOtpCode).WrapWrongOtpCode().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrWrongOtpCode)
		return
	}

	err = o.repositoryRegistry.OTPRepo.Delete(repoModels.OtpGUID(jsonRequest.OtpGUID))
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	account, err := o.repositoryRegistry.AccountsRepo.GetByLogin(jsonRequest.Login)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	sessionRepo := o.sessionGenerate(sessionGenerateDep{
		Request:     r,
		AccountGUID: account.AccountGUID,
	})

	err = o.repositoryRegistry.SessionsRepo.Create(sessionRepo)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.SessionCreateRepo2Client(sessionRepo, account)
	writeJson(w, logger, response)
}

func (o *AuthHandlers) Logout(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Logout")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	account, exist := GetAccountFromContext(r.Context())
	if !exist {
		exception := errproc.NewException(ErrAuthIsRequired).WrapAuthIsRequired().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrAuthIsRequired)
		return
	}
	errproc.AddAccountData(account)

	session, exist := GetSessionFromContext(r.Context())
	if !exist {
		exception := errproc.NewException(ErrAuthIsRequired).WrapSessionErr().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrAuthIsRequired)
		return
	}

	err = o.repositoryRegistry.SessionsRepo.Delete(session)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	writeJson(w, logger, nil)
}

func (o *AuthHandlers) RefreshSession(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "RefreshSession")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var request models.RefreshSessionReq
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(requestBody, &request)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	session := o.repositoryRegistry.SessionsRepo.GetRefresh(request.Rt)
	if session == nil {
		exception := errproc.NewException(ErrSessionRefresh).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrSessionRefresh)
		return
	}

	if time.Now().After(time.Unix(session.RefreshTokenExp, 0)) {
		exception := errproc.NewException(ErrSessionRefreshTokenExpired).WrapRefreshTokenExp().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrSessionRefreshTokenExpired)
		return
	}

	if session.AccountGUID != request.LinkObjectGUID {
		exception := errproc.NewException(ErrRequestAndSessionDataAreDiff).WrapSessionCompareRequest().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusUnauthorized, ErrRequestAndSessionDataAreDiff)
		return
	}

	err = o.repositoryRegistry.SessionsRepo.Delete(session)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	sessionRepo := o.sessionGenerate(sessionGenerateDep{
		Request:     r,
		AccountGUID: session.AccountGUID,
	})

	err = o.repositoryRegistry.SessionsRepo.Create(sessionRepo)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.SessionRefreshRepo2Client(sessionRepo)
	writeJson(w, logger, response)
}

func (o *AuthHandlers) ChangeProfileImage(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "ChangeProfileData")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	account, exist := GetAccountFromContext(r.Context())
	if !exist {
		exception := errproc.NewException(ErrAuthIsRequired).WrapAuthIsRequired().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrAuthIsRequired)
		return
	}
	errproc.AddAccountData(account)

	viper.SetEnvPrefix("imageservice")
	err = viper.BindEnv("address")
	if err != nil {
		exception := errproc.NewException(ErrBindEnv).WrapBindEnvError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrBindEnv)
		return
	}

	resp, err := integrations.RedirectUploadImageFile(viper.GetString("address"), r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapCloseFileError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		o.logger.Errorf("Error reading response body: %v", err.Error())
		return
	}

	account.ImageURL = convert.ImageServiceUploadImageRes2Client(bodyBytes)

	err = o.repositoryRegistry.AccountsRepo.Upsert(account)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.AccountRepo2Client(account)
	writeJson(w, logger, response)
}

func (o *AuthHandlers) ChangeProfileData(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "ChangeProfileData")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	account, exist := GetAccountFromContext(r.Context())
	if !exist {
		exception := errproc.NewException(ErrAuthIsRequired).WrapAuthIsRequired().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrAuthIsRequired)
		return
	}
	errproc.AddAccountData(account)

	var request models.ChangeProfileDataReq
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(requestBody, &request)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	if request.Name != "" {
		account.Name = request.Name
	}
	if request.Surname != "" {
		account.Surname = request.Surname
	}
	if request.City != "" {
		account.City = request.City
	}
	account.Status = 0
	account.Role = repoModels.RoleUser

	err = o.repositoryRegistry.AccountsRepo.Upsert(account)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.AccountRepo2Client(account)
	writeJson(w, logger, response)
}

func (o *AuthHandlers) GetCities(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "ChangeProfileData")
	_, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.Cities2Client(repoModels.Cities)
	writeJson(w, logger, response)
}

func (o *AuthHandlers) GetByAccountGUID(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetByAccountGUID")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	accountGUID := vars["AccountGUID"]

	account, err := o.repositoryRegistry.AccountsRepo.GetByAccountGUID(accountGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.User2Client(account)
	writeJson(w, logger, response)
}

func (o *AuthHandlers) GetByAddress(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetByAddress")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var decoder = schema.NewDecoder()
	var args models.GetUsersReq
	err = decoder.Decode(&args, r.URL.Query())
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		return
	}

	accounts := o.repositoryRegistry.AccountsRepo.GetByAddress(args.City)

	response := convert.Users2Client(accounts)
	writeJson(w, logger, response)
}

func (o *AuthHandlers) MakeRestAdmin(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "MakeRestAdmin")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	AccountGUID := vars["AccountGUID"]

	profile, err := o.repositoryRegistry.AccountsRepo.GetByAccountGUID(AccountGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	profile.RestAdmin = true

	err = o.repositoryRegistry.AccountsRepo.Upsert(profile)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
}

func (o *AuthHandlers) GetProfile(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetProfile")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	account, exist := GetAccountFromContext(r.Context())
	if !exist {
		exception := errproc.NewException(ErrAuthIsRequired).WrapAuthIsRequired().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrAuthIsRequired)
		return
	}
	errproc.AddAccountData(account)

	response := convert.AccountRepo2Client(account)
	writeJson(w, logger, response)
}

type sessionGenerateDep struct {
	Request     *http.Request
	AccountGUID string
}

func (o *AuthHandlers) sessionGenerate(dep sessionGenerateDep) *repoModels.Session {
	generationTime := time.Now()
	AccessToken := uuid.NewString()
	AccesTokenExp := generationTime.Add(time.Duration(o.conf.AuthSettings.AccessTokenExpiration)).Unix()
	RefreshToken := uuid.NewString()
	RefreshTokenExp := generationTime.Add(time.Duration(o.conf.AuthSettings.RefreshTokenExpiration)).Unix()

	return &repoModels.Session{
		CookiesKey:      AccessToken,
		AccountGUID:     dep.AccountGUID,
		ExpiredTime:     AccesTokenExp,
		DeviceName:      dep.Request.UserAgent(),
		DeviceID:        "",
		RefreshToken:    RefreshToken,
		RefreshTokenExp: RefreshTokenExp,
		System:          "",
	}
}
