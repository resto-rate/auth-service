package handlers

import (
	"AuthService/internal/config"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"net/http"
)

type AuthI interface {
	Health(w http.ResponseWriter, r *http.Request)
	Methods(w http.ResponseWriter, r *http.Request)
	GetOTPCode(w http.ResponseWriter, r *http.Request)
	LoginOTPCode(w http.ResponseWriter, r *http.Request)
	Logout(w http.ResponseWriter, r *http.Request)
	RefreshSession(w http.ResponseWriter, r *http.Request)
	ChangeProfileData(w http.ResponseWriter, r *http.Request)
	GetProfile(w http.ResponseWriter, r *http.Request)
	ChangeProfileImage(w http.ResponseWriter, r *http.Request)
	LoginTgBot(w http.ResponseWriter, r *http.Request)
	MakeRestAdmin(w http.ResponseWriter, r *http.Request)
	GetCities(w http.ResponseWriter, r *http.Request)
	GetByAccountGUID(w http.ResponseWriter, r *http.Request)
	GetByAddress(w http.ResponseWriter, r *http.Request)
	/*Login(w http.ResponseWriter, r *http.Request)
	LoginPartner(w http.ResponseWriter, r *http.Request)
	LogoutPartner(w http.ResponseWriter, r *http.Request)
	ChangeProfile(w http.ResponseWriter, r *http.Request)
	*/
}

type MiddlewareI interface {
	Middleware(next http.Handler) http.Handler
}

type Profile interface {
	Profile(w http.ResponseWriter, r *http.Request)
}

type OtpI interface {
	Search(w http.ResponseWriter, r *http.Request)
}

type ConfI interface {
	Get(w http.ResponseWriter, r *http.Request)
}

type Middlewares struct {
	RequestIDMiddleware MiddlewareI
	LimiterMiddleware   MiddlewareI
	AuthMiddleware      MiddlewareI
}

type Handlers struct {
	Profile Profile
}

type RouteDependencies struct {
	Logger      logrus.FieldLogger
	Middlewares Middlewares
	Handlers    Handlers

	Otp OtpI

	Auth AuthI

	Conf *config.Config
}

type RouteOptions struct {
	Version string
	Cors    RouteOptionsCors
}

type RouteOptionsCors struct {
	AllowedOrigins   []string
	AllowedHeaders   []string
	AllowCredentials bool
	Debug            bool
}

func NewRoute(dep RouteDependencies, opt RouteOptions) http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/health", dep.Auth.Health)

	auth := r.PathPrefix("/auth").Subrouter()
	auth.Use(dep.Middlewares.RequestIDMiddleware.Middleware)
	profile := auth.PathPrefix("/profile").Subrouter()
	profile.Use(dep.Middlewares.AuthMiddleware.Middleware)

	if dep.Conf.PortalSecurity.Limiter.Enabled {
		auth.Use(dep.Middlewares.LimiterMiddleware.Middleware)
	}

	// swagger:route GET /auth/methods authMethods authMethods
	//
	// Return all available auth methods
	//
	//     Parameters: []
	//
	//     Responses:
	//       200:
	//		 	description: status ok
	//			schema:
	//				$ref: "#/model/AuthMethodsResponse"
	//       500:
	auth.HandleFunc("/methods", dep.Auth.Methods).Methods(http.MethodGet)

	auth.HandleFunc("/tlg", dep.Auth.LoginTgBot).Methods(http.MethodPost)

	// swagger:route POST /auth/otp getOtpCode getOtpCode
	//
	//	Send to user otp code to phone by sms (or just to terminal)
	//
	//  parameters:
	//		+ name: Login
	//      in: body
	//		type: GetOTPCodeReq
	//      description: user login (phone number)
	//      required: true
	//      allowEmpty:  false
	//
	//	responses:
	//  	200:body:GetOTPCodeRes
	//      400:
	//      401:
	auth.HandleFunc("/otp", dep.Auth.GetOTPCode).Methods(http.MethodPost)

	auth.HandleFunc("/users/{AccountGUID}", dep.Auth.GetByAccountGUID).Methods(http.MethodGet)

	// swagger:route POST /auth/loginotp loginotp loginotp
	//
	//	Enter to service with otp
	//
	//  parameters:
	//		+ name: LoginOTPCodeReq
	//      in: body
	//		type: LoginOTPCodeReq
	//      description: params to login
	//      required: true
	//      allowEmpty:  false
	//
	//	responses:
	//  	200:body:LoginResponse
	//		500:
	// 		400:
	// 		403:
	auth.HandleFunc("/loginotp", dep.Auth.LoginOTPCode).Methods(http.MethodPost)

	auth.HandleFunc("/cities", dep.Auth.GetCities).Methods(http.MethodGet)
	auth.HandleFunc("/makerestadmin/{AccountGUID}", dep.Auth.MakeRestAdmin).Methods(http.MethodGet)
	auth.HandleFunc("/getbyaddress", dep.Auth.GetByAddress).Methods(http.MethodGet)

	// swagger:route DELETE /auth/profile/logout logout logout
	//
	//	Enter to service with otp
	//
	//  parameters:
	//		+ name: AccessToken
	//      in: header
	//		type: string
	//      description: access token to close session
	//      required: true
	//      allowEmpty:  false
	//
	//	responses:
	//  	200:
	//      500:
	//  	400:
	profile.HandleFunc("/logout", dep.Auth.Logout).Methods(http.MethodDelete)

	// swagger:route POST /auth/profile/refauth refauth refauth
	//
	//	Refresh token
	//
	//  parameters:
	//		+ name: RefreshSessionReq
	//      in: body
	//		type: RefreshSessionReq
	//      description: params to refresh session
	//      required: true
	//      allowEmpty: false
	//
	//	responses:
	//  	200:body:RefreshResponse
	//		500:
	// 		400:
	// 		403:
	profile.HandleFunc("/refauth", dep.Auth.RefreshSession).Methods(http.MethodPost)

	// swagger:route POST /auth/profile/changeProfileData changeProfileData changeProfileData
	//
	//  parameters:
	//		+ name: ChangeProfileDataReq
	//      in: body
	//		type: ChangeProfileDataReq
	//      description: params to change profile data
	//      required: true
	//      allowEmpty: false
	//
	//	responses:
	//  	200:body:AccountResponse
	//		500:
	// 		400:
	// 		403:
	profile.HandleFunc("/changeProfileData", dep.Auth.ChangeProfileData).Methods(http.MethodPost)

	// swagger:route POST /auth/profile/changeImage changeProfileImage changeProfileImage
	//
	//  parameters:
	//		+ name: imageFile
	//      in: body
	//      description: image file
	//      required: true
	//      allowEmpty: false
	//
	//	responses:
	//  	200:body:AccountResponse
	//		500:
	// 		400:
	// 		403:
	profile.HandleFunc("/changeImage", dep.Auth.ChangeProfileImage).Methods(http.MethodPost)

	// swagger:route GET /auth/profile/data getProfile getProfile
	//
	//
	//	responses:
	//  	200:body:AccountResponse
	//		500:
	// 		400:
	// 		403:
	profile.HandleFunc("/data", dep.Auth.GetProfile).Methods(http.MethodGet)

	corsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Origin", "Authorization", "Content-Type"},
	})

	return corsHandler.Handler(r)
}
