package handlers

import (
	"AuthService/internal/config"
	"AuthService/internal/repo"
	"errors"
	"github.com/sirupsen/logrus"
	"net/http"
)

type OtpHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
}

// Структура зависимостей
type OtpDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
}

// Инициализация обработчиков
func NewOtpHandlers(dep OtpDependencies) (*OtpHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, errors.New("dep RepositoryRegistry is null")
	}
	if dep.Logger == nil {
		return nil, errors.New("dep Logger is null")
	}
	if dep.Conf == nil {
		return nil, errors.New("dep Conf is null")
	}

	logger := dep.Logger.WithField("AuthService", "OtpHandlers")

	return &OtpHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
	}, nil
}

func (o *OtpHandlers) Search(w http.ResponseWriter, r *http.Request) {

	/*logger := o.logger

	account, err := ctxGetAccount(r.Context())
	if err != nil {
		err := WriteError(w, fmt.Errorf("user not authorized err_4FC"), http.StatusForbidden)
		if err != nil {
			logger.WithError(err).Error("OTPHandlers Search err_85D")
		}
		return
	}

	// Закрываем доступ к обработчику
	if account.Role != web.RoleRootAdmin {
		logger.Error("attempted to create a response without the appropriate user rights err_4FE")
		err := WriteError(w, fmt.Errorf("attempted to create a response without the appropriate user rights err_4FF"), http.StatusForbidden)
		if err != nil {
			logger.WithError(err).Error("OTPHandlers Search err_85E")
		}
		return
	}

	var args modelscorplk.OtpSearchArgs
	var decoder = schema.NewDecoder()

	// Получаем параметры Get-запроса
	err = decoder.Decode(&args, r.URL.Query())
	if err != nil {
		logger.WithError(err).Error("OTPHandlers Search err_85F")
	}

	dbargs := args.JSONConv()
	OtpCodes, err := o.repositoryRegistry.OTPRepo.Search(dbargs)
	if err != nil {
		logger.WithError(err).Error("otp repoFunctions Search err_860")
		// Возвращаме ошибку в ответ на запрос
		err := WriteError(w, err, http.StatusBadRequest)
		if err != nil {
			logger.WithError(err).Error("OTPHandlers Search err_861")
		}
		return
	}

	var webModels modelscorplk.OtpSearchResponseADMR
	webModels.JSONConv(OtpCodes)

	// Записываем результат в ответ
	err = WriteJson(w, webModels)
	if err != nil {
		logger.WithError(err).Error("OTPHandlers Search err_862")
	}
	*/
}
