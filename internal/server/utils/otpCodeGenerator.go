package utils

import (
	"crypto/rand"
	"fmt"
	"math/big"
)

type OTP struct {
	Code      string
	CodeSpace string
}

func (o *OTP) GenerateCode(min, max int64) OTP {

	bg := big.NewInt(max - min)
	bigMin := big.NewInt(min)

	n, err := rand.Int(rand.Reader, bg)
	if err != nil {
		panic(err)
	}
	n = n.Add(n, bigMin)

	code := n.String()

	codeSpace := code
	if len(code) > 5 {
		codeSpace = fmt.Sprintf("%s %s", code[:3], code[3:])
	}

	return OTP{
		Code:      code,
		CodeSpace: codeSpace,
	}
}

func (o *OTP) DefaultCode() OTP {
	return OTP{
		Code:      "638592",
		CodeSpace: "638 592",
	}
}
