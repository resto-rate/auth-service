package middleware

import (
	"AuthService/internal/config"
	"AuthService/internal/repo"
	"AuthService/internal/repo/repoModels"
	"context"
	"errors"
	"net/http"
)

type AuthMiddleware struct {
	repositoryRegistry *repo.Registry
	conf               *config.Config
}

type AuthMiddlewareDependencies struct {
	RepositoryRegistry *repo.Registry
	Conf               *config.Config
}

type AuthMiddlewareOptions struct{}

func NewAuthMiddleware(dep AuthMiddlewareDependencies) (*AuthMiddleware, error) {

	if dep.RepositoryRegistry == nil {
		return nil, errors.New("dep RepositoryRegistry is null")
	}
	if dep.Conf == nil {
		return nil, errors.New("dep Conf is null")
	}
	return &AuthMiddleware{
		repositoryRegistry: dep.RepositoryRegistry,
		conf:               dep.Conf,
	}, nil
}

func (mw *AuthMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		accessToken := r.Header.Get("Authorization")
		if accessToken == "" {
			// Токен не передан
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		// Получаем данные пользователя
		session, err := mw.repositoryRegistry.SessionsRepo.Get(accessToken)

		if errors.Is(err, repo.ErrSessionExpired) && mw.conf.AuthSettings.AccessTokenExpiration > 0 {
			// Сессия истекла
			w.WriteHeader(http.StatusForbidden)
			return
		}

		if err != nil && !errors.Is(err, repo.ErrSessionExpired) {
			// Сессия не найдена
			w.WriteHeader(http.StatusForbidden)
			return
		}

		// Возвращаем данные пользователя
		account, err := mw.repositoryRegistry.AccountsRepo.GetByAccountGUID(session.AccountGUID)
		if err != nil {
			// TODO: Добавить обработчик ошибок
			//handlers.WriteError(w, err, http.StatusInternalServerError)
			return
		}

		if account.Status != repoModels.AccountsStatus.Active && account.Status != repoModels.AccountsStatus.TempAccount {
			// TODO: Добавить обработчик ошибок
			//handlers.WriteError(w, errors.New("account is blocked"), http.StatusForbidden)
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), CtxKeySession, session))

		// Добавляем данные пользователя в контекст
		r = r.WithContext(context.WithValue(r.Context(), CtxKeyAccount, account))
		next.ServeHTTP(w, r)

	})
}
