package middleware

import (
	"AuthService/internal/server/utils"
	"errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
	"net/http"
)

type LimiterMiddleware struct {
	limiter *utils.IPRateLimiter
	logger  logrus.FieldLogger
}

// Проверка на превышение лимита
func (mw *LimiterMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		userIP, err := utils.GetClientIP(r)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		// Проверк на превышение лимита
		hostLimiter := mw.limiter.GetLimiter(userIP)
		if !hostLimiter.Allow() {
			err := hardClose(w)
			if err != nil {
				mw.logger.WithError(err).Error("hardClose error")
			}
			return
		}

		// Проверяем наличие заголовка Content-Length, у методов не подразумевающих наличие тела
		// if r.Method == http.MethodGet || r.Method == http.MethodHead || r.Method == http.MethodOptions || r.Method == http.MethodDelete {
		if r.Method == http.MethodGet || r.Method == http.MethodHead || r.Method == http.MethodOptions {
			if len(r.Header.Get("Content-Length")) > 0 {
				err := hardClose(w)
				if err != nil {
					mw.logger.WithError(err).Error("hardClose error")
				}
			}
		}

		// Проверяем наличие заголовка Range
		if len(r.Header.Get("Range")) > 0 {

			err := hardClose(w)
			if err != nil {
				mw.logger.WithError(err).Error("hardClose error")
			}
		}

		// Переход к следующему шагу
		next.ServeHTTP(w, r)
	})
}

type LimiterMiddlewareDependencies struct {
	Logger logrus.FieldLogger
}

type LimiterMiddlewareOptions struct {
	RequestPerSecond float64
	MaxOnlineRequest int
}

func NewLimiterMiddleware(dep LimiterMiddlewareDependencies, RequestPerSecond float64, MaxOnlineRequest int) (*LimiterMiddleware, error) {

	if dep.Logger == nil {
		return nil, errors.New("dep Logger is null")
	}

	return &LimiterMiddleware{
		limiter: utils.NewIPRateLimiter(rate.Limit(RequestPerSecond), MaxOnlineRequest),
		logger:  dep.Logger,
	}, nil
}
