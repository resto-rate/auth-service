package repoFunctions

import (
	"AuthService/internal/config"
	"AuthService/internal/repo"
	"AuthService/internal/repo/repoModels"
	"github.com/Restream/reindexer"
	"strings"
)

type AccountsRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewAccountsRepo(db *reindexer.Reindexer) *AccountsRepo {
	return &AccountsRepo{
		db: db,
	}
}

func (o *AccountsRepo) Create(arg *repoModels.AccountCreateArg) error {

	user := &repoModels.AccountCreateArg{
		Login:           arg.Login,
		Mobile:          arg.Mobile,
		Username:        arg.Username,
		MaxSessionCount: arg.MaxSessionCount,
		Password:        arg.Password,
		Salt:            arg.Salt,
		Role:            arg.Role,
		TimeZone:        arg.TimeZone,
		LinkObjectGUID:  arg.LinkObjectGUID,
		LinkObjectType:  arg.LinkObjectType,
		SubdivisionGUID: arg.SubdivisionGUID,
	}

	_, err := o.db.Insert(repo.TableAccounts, user, "UserID=Serial()")
	if err != nil {
		return err
	}

	return nil
}

func (o *AccountsRepo) GetByLogin(login string) (*repoModels.Account, error) {
	elem, found := o.db.Query(repo.TableAccounts).WhereString("Login", reindexer.EQ, login).
		OpenBracket().
		WhereInt64("Status", reindexer.EQ, repoModels.AccountsStatus.Active).
		Or().
		WhereInt64("Status", reindexer.EQ, repoModels.AccountsStatus.TempAccount).
		CloseBracket().
		Get()

	// Объект найден
	if found {
		model := elem.(*repoModels.Account)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}

func (o *AccountsRepo) GetByAccountGUID(guid string) (*repoModels.Account, error) {
	elem, found := o.db.Query(repo.TableAccounts).WhereString("AccountGUID", reindexer.EQ, guid).
		OpenBracket().
		WhereInt64("Status", reindexer.EQ, repoModels.AccountsStatus.Active).
		Or().
		WhereInt64("Status", reindexer.EQ, repoModels.AccountsStatus.TempAccount).
		CloseBracket().
		Get()

	// Объект найден
	if found {
		model := elem.(*repoModels.Account)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}

func (o *AccountsRepo) GetByAddress(address string) repoModels.AccountGUIDsSlice {
	accounts := o.db.Query(repo.TableAccounts).Exec()
	result := repoModels.AccountGUIDsSlice{}

	for accounts.Next() {
		item := accounts.Object().(*repoModels.Account)
		if strings.Contains(address, item.City) {
			result = append(result, repoModels.AccountGUID(item.AccountGUID))
		}
	}

	return result
}

func (o *AccountsRepo) Upsert(account *repoModels.Account) error {
	return o.db.Upsert(repo.TableAccounts, account)
}
