package repoFunctions

import (
	"AuthService/internal/repo"
	"AuthService/internal/repo/repoModels"
	"github.com/Restream/reindexer"
)

type SessionsRepo struct {
	db *reindexer.Reindexer
}

func NewSessionRepo(db *reindexer.Reindexer) *SessionsRepo {
	return &SessionsRepo{
		db: db,
	}
}

func (o *SessionsRepo) Create(model *repoModels.Session) error {
	_, err := o.db.Insert(repo.TableSessions, model)
	if err != nil {
		return err
	}

	return nil
}

func (o *SessionsRepo) Delete(model *repoModels.Session) error {
	return o.db.Delete(repo.TableSessions, model)
}

func (o *SessionsRepo) GetRefresh(refreshToken string) *repoModels.Session {
	elem, found := o.db.Query(repo.TableSessions).
		WhereString("RefreshToken", reindexer.EQ, refreshToken).
		Get()

	if found {
		model := elem.(*repoModels.Session)
		return model
	}

	return nil
}

func (o *SessionsRepo) Get(key string) (*repoModels.Session, error) {
	elem, found := o.db.Query(repo.TableSessions).
		WhereString("CookiesKey", reindexer.EQ, key).Get()

	if found {
		model := elem.(*repoModels.Session)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}
