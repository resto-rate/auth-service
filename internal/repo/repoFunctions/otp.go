package repoFunctions

import (
	"AuthService/internal/repo"
	"AuthService/internal/repo/repoModels"
	"github.com/Restream/reindexer"
)

type OTPRepo struct {
	db *reindexer.Reindexer
}

func NewOTPRepo(db *reindexer.Reindexer) *OTPRepo {
	return &OTPRepo{
		db: db,
	}
}

func (o *OTPRepo) CountPhone(phone *repoModels.OtpPhone) int {
	iterator := o.db.Query(repo.TableOTP).WhereString("Phone", reindexer.EQ, string(*phone)).ReqTotal().Exec()

	defer iterator.Close()

	return iterator.TotalCount()
}

func (o *OTPRepo) Create(otpObject repoModels.OTP) error {

	err := o.db.Upsert(repo.TableOTP, otpObject)
	if err != nil {
		return err
	}

	return nil
}

func (o *OTPRepo) Get(otpGUID repoModels.OtpGUID) (*repoModels.OTP, bool) {
	otp, found := o.db.Query(repo.TableOTP).WhereString("OtpGUID", reindexer.EQ, string(otpGUID)).WhereInt64("RemainingAttempts", reindexer.GT, 0).Get()
	if found {
		return otp.(*repoModels.OTP), found
	}

	return nil, false
}

func (o *OTPRepo) Delete(otpGUID repoModels.OtpGUID) error {
	_, err := o.db.Query(repo.TableOTP).WhereString("OtpGUID", reindexer.EQ, string(otpGUID)).Delete()
	return err
}
