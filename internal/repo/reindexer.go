package repo

import (
	"AuthService/internal/repo/repoModels"
	"errors"
	"fmt"
	"github.com/Restream/reindexer"
)

var (
	ErrorNotFound     = errors.New("not found")
	ErrSessionExpired = errors.New("session expired")
)

const (
	TableAccounts = "accounts"
	TableOTP      = "otp"
	TableSessions = "sessions"
)

var Tables map[string]interface{}

func init() {
	Tables = make(map[string]interface{})
	Tables[TableAccounts] = repoModels.Account{}
	Tables[TableOTP] = repoModels.OTP{}
	Tables[TableSessions] = repoModels.Session{}
}

func NewReindexerConnect(connect string) (*reindexer.Reindexer, error) {
	var err error

	// Подключение к БД
	reindexerConnect := reindexer.NewReindex(connect, reindexer.WithCreateDBIfMissing())

	// Проверяем состояние
	if reindexerConnect.Status().Err != nil {
		return nil, fmt.Errorf("NewDBConnect Status - (%w)", reindexerConnect.Status().Err)
	}

	// Производим открытие таблиц Reindexer
	for TableName, TableStruct := range Tables {
		err := reindexerConnect.OpenNamespace(TableName, reindexer.DefaultNamespaceOptions(), TableStruct)
		if err != nil {
			return nil, fmt.Errorf("NewDBConnect OpenNamespace - %s (%w)", TableName, err)
		}
	}

	// Возвращаем соединение
	return reindexerConnect, err
}

type Registry struct {
	repoModels.AccountsRepo
	repoModels.OTPRepo
	repoModels.SessionsRepo
}
