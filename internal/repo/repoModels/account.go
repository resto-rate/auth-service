package repoModels

type AccountsRepo interface {
	GetByLogin(login string) (*Account, error)
	GetByAccountGUID(guid string) (*Account, error)
	Upsert(account *Account) error
	GetByAddress(address string) AccountGUIDsSlice
	/*Auth(username string, password string) (*Account, error)
	GetToAccountGUID(AccountGUID) (*Account, error)
	UpsertAsync(users map[string]*Account) (err error)
	GetFromAccountGUID(accountGUIDs map[string]*string) map[string]*Account
	Search(arg *AccountsSearchArg) (*AccountSearchResult, error)
	Create(arg *AccountCreateArg) error
	Update(arg *Account) error
	GetAll() map[string]*Account
	GetAllForRole(role string) map[string]*Account*/
}

const (
	RoleUser  = "USER"
	RoleAdmin = "ADMIN"
	RoleTemp  = "TEMP"

	EnterTypeSms = "EnterTypeSms"
	EnterTypeTg  = "EnterTypeTg"
)

var (
	Cities []string
)

func init() {
	Cities = []string{"Москва", "Санкт-Петербург", "Новосибирск", "Екатеринбург", "Казань", "Нижний Новгород",
		"Челябинск", "Самара", "Уфа", "Ростов-на-Дону", "Омск", "Краснодар", "Воронеж", "Волгоград",
		"Пермь", "Красноярск", "Саратов", "Краснодар", "Тольятти", "Ижевск", "Ульяновск", "Барнаул",
		"Владивосток", "Ярославль", "Иркутск", "Тюмень", "Махачкала", "Хабаровск", "Новокузнецк", "Оренбург", "Кемерово",
		"Рязань", "Томск", "Астрахань", "Пенза", "Набережные Челны", "Липецк", "Тула"}
}

type AccountGUID string
type AccountGUIDsSlice []AccountGUID

type Account struct {
	AccountGUID     string `reindex:"AccountGUID,hash,pk"` // Идентификатор связанного объекта
	Status          int64  `reindex:"Status,hash"`         // Статус аккаунта
	Login           string `reindex:"Login,hash"`          // Имя пользователя
	Mobile          string `reindex:"Mobile,hash"`         // Мобильный телефон
	Name            string `reindex:"Name,hash"`           // Имя пользователя
	Surname         string `reindex:"Surname,hash"`        // Фамилия пользователя
	ImageURL        string `reindex:"ImageURL"`            // Ссылка на картинку пользователя
	MaxSessionCount int64  `reindex:"MaxSessionCount"`     // Максимальное количество сессий
	Role            string `reindex:"Role"`                // Роль учетной записи
	TimeZone        int64  `reindex:"TimeZone"`            // Часовой пояс
	Blocked         bool   `reindex:"Blocked"`
	EnterType       string `reindex:"EnterType"` // Тип входа в акк [sms - вход по коду из смс, tg - вход по коду]
	ChatID          string `reindex:"ChatID"     // Идентификатор чата в телеграмме`
	RestAdmin       bool   `reindex:"RestAdmin"`
	City            string `reindex:"City,hash"`
}

type AccountsSearchArg struct {
	Page           int
	OrderBy        string
	OrderDirection int
	Login          string
	Mobile         string
	Username       string
	Role           string
}

type AccountSearchResult struct {
	Users []*Account
}

type AccountCreateArg struct {
	Login           string
	Mobile          string
	Email           string
	Username        string
	MaxSessionCount int64
	Password        string
	Salt            string
	Role            string
	TimeZone        int64
	LinkObjectGUID  string
	LinkObjectType  string
	SubdivisionGUID string
}

var AccountsStatus struct {
	Active                                     int64 // Активен
	ActiveWithRestrictions                     int64 // Активен с ограничениями
	TempAccount                                int64
	PhoneDuplicate                             int64
	DoubleBondWithPartnerAndCompany            int64
	ObjectDoesNotContainLinkToPartnerOrCompany int64
	NoAssignedRoleForPosition                  int64
	Blocked                                    int64
}

type AccountStatusString string

var AccountStatuses map[int64]AccountStatusString

func init() {
	AccountsStatus.Active = 0
	AccountsStatus.TempAccount = 1
	AccountsStatus.PhoneDuplicate = 2
	AccountsStatus.DoubleBondWithPartnerAndCompany = 3
	AccountsStatus.ObjectDoesNotContainLinkToPartnerOrCompany = 4
	AccountsStatus.NoAssignedRoleForPosition = 5
	AccountsStatus.Blocked = 6
	AccountsStatus.ActiveWithRestrictions = 7

	AccountStatuses = make(map[int64]AccountStatusString)
	AccountStatuses[AccountsStatus.Active] = AccountStatusString("Активный")
	AccountStatuses[AccountsStatus.ActiveWithRestrictions] = AccountStatusString("Активный с ограничениями")
	AccountStatuses[AccountsStatus.TempAccount] = AccountStatusString("Временный аккаунт")
	AccountStatuses[AccountsStatus.PhoneDuplicate] = AccountStatusString("Дублированный номер телефона")
	AccountStatuses[AccountsStatus.DoubleBondWithPartnerAndCompany] = AccountStatusString("Дублированная связь с партнером и компанией")
	AccountStatuses[AccountsStatus.ObjectDoesNotContainLinkToPartnerOrCompany] = AccountStatusString("Объект не содержит ссылки на партнера или компанию")
	AccountStatuses[AccountsStatus.NoAssignedRoleForPosition] = AccountStatusString("Не назначенная роль на позицию")
	AccountStatuses[AccountsStatus.Blocked] = AccountStatusString("Заблокирован")
}
