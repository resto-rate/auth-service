package repoModels

type SessionsRepo interface {
	Create(*Session) error
	Delete(session *Session) error
	GetRefresh(refreshToken string) *Session
	Get(key string) (*Session, error)
	/*SetManagerGUID(cookiesKey, LinkObjectGUID, ManagerGUID string) bool
	GetAll() []*Session
	UpsertAsync(objects *SessionModelSlice) (err error)
	RemoveSession(*Session) error
	UpsertSession(*Session) error
	*/
}

type SessionModelSlice []Session

type Session struct {
	CookiesKey      string `reindex:"CookiesKey,hash,pk"`
	AccountGUID     string `reindex:"LinkObjectGUID,hash"`
	ExpiredTime     int64  `reindex:"ExpiredTime"`
	DeviceName      string `reindex:"DeviceName"`
	DeviceID        string `reindex:"DeviceID"`
	RefreshToken    string `reindex:"RefreshToken,hash"`
	RefreshTokenExp int64  `reindex:"RefreshTokenExp,hash"`
	System          string `reindex:"System,hash"`
}
