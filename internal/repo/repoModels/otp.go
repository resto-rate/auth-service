package repoModels

type OTPRepo interface {
	Create(OTP) error
	Get(otpGUID OtpGUID) (*OTP, bool)
	//CountApp(objectID, DeviceID string) int
	CountPhone(*OtpPhone) int
	Delete(otpGUID OtpGUID) error
	//MinusAttempt(otpGUID OtpGUID, NewRemainingAttempts int64)
}

type OtpGUID string
type OtpPhone string

type OTP struct {
	OtpGUID           string   `reindex:"OtpGUID,hash,pk"`   // Идентификатор связанного объекта
	Phone             string   `reindex:"Phone,hash"`        // Телефон на который отправлено сообщение
	ObjectType        string   `reindex:"ObjectType"`        // Тип связанного объекта
	OtpCode           string   `reindex:"OtpCode,hash"`      // OTP-Код
	Expired           int64    `reindex:"Expired,ttl"`       // Срок жизни
	Created           int64    `reindex:"Created"`           // Время создания кода
	RemainingAttempts int64    `reindex:"RemainingAttempts"` // Оставшиеся попытки неправильного ввода кода
	DeviceID          string   `reindex:"DeviceID,hash"`     // Идентификатор устройства для авторизации на котором запрашивается код
	AccountIDs        []string // Идентификаторы связанных объектов
}
