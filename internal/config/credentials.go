package config

import (
	"github.com/spf13/viper"
)

type Credentials struct {
	Reindexer string
}

func NewCredentials() (*Credentials, error) {
	var credentials Credentials

	err := viper.BindEnv("db")
	if err != nil {
		return nil, err
	}

	credentials.Reindexer = viper.GetString("db")

	return &credentials, nil
}
