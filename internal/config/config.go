package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"time"
)

const (
	configPath = "."
	configName = "conf"
)

type Config struct {
	Logger       logger       `yaml:"Logger"`
	WebServer    webServer    `yaml:"WebServer"`
	AuthSettings authSettings `yaml:"AuthSettings"`

	// Общие настройки
	FullTextItemCount int // Количество выдаваемых вариантов полнотекстового поиска
	MinLenFindString  int // Минимальное количество символов для начала поиска

	PortalSecurity struct {
		Limiter        Limiter
		SessionExpired int64
	}

	ImageServiceAddress string
	TgBotAddress        string
}

func NewConfiguration() (*Config, error) {
	viper.AddConfigPath(configPath)
	viper.SetConfigName(configName)

	var configuration Config

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	err = viper.Unmarshal(&configuration)
	if err != nil {
		return nil, err
	}

	viper.SetEnvPrefix("authservice")
	err = viper.BindEnv("port")
	if err != nil {
		return nil, err
	}

	configuration.WebServer.ListenPorts.Http = viper.GetString("port")

	return &configuration, nil
}

type webServer struct {
	ListenPorts struct {
		Http string `yaml:"Http"`
	} `yaml:"ListenPorts"`
	Configuration struct {
		ReadTimeout       time.Duration `yaml:"ReadTimeout"`
		ReadHeaderTimeout time.Duration `yaml:"ReadHeaderTimeout"`
		WriteTimeout      time.Duration `yaml:"WriteTimeout"`
		IdleTimeout       time.Duration `yaml:"IdleTimeout"`
		MaxHeaderBytes    int           `yaml:"MaxHeaderBytes"`
		ShutdownTimeout   time.Duration `yaml:"ShutdownTimeout"`
	} `yaml:"Configuration"`
	CORS struct {
		AllowedOrigins   []string `yaml:"AllowedOrigins"`
		AllowCredentials bool     `yaml:"AllowCredentials"`
		AllowedHeaders   []string `yaml:"AllowedHeaders"`
		Debug            bool     `yaml:"Debug"`
	} `yaml:"CORS"`
}

type authSettings struct {
	AccessTokenExpiration  uint32 `yaml:"AccessTokenExpiration"`
	RefreshTokenExpiration uint32 `yaml:"RefreshTokenExpiration"`
	OTP                    struct {
		OTPEnabled           bool  `yaml:"OTPEnabled"`
		MaxRemainingAttempts int64 `yaml:"MaxRemainingAttempts"`
		LogRetentionTime     int64 `yaml:"LogRetentionTime"`
		Auth                 struct {
			SendText       string `yaml:"SendText"`
			CodeFrom       int64  `yaml:"CodeFrom"`
			CodeTo         int64  `yaml:"CodeTo"`
			ExpiredSeconds int64  `yaml:"ExpiredSeconds"`
		} `yaml:"Auth"`
	} `yaml:"OTP"`
}

type logger struct {
	Level logrus.Level
	Type  string
}

type Limiter struct {
	Enabled          bool
	RequestPerSecond float64
	MaxOnlineRequest int
}
